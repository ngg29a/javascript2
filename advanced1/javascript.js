function changeColor(){
    let paragraph = document.getElementById("paragraph");
    let colorName = document.getElementById("color");
    let value = colorName.value;
    
    // change the color of paragraph
    paragraph.style.color = value;

}
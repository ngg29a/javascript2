function findGCR(number1, number2){
    let result;

    // if one of the number is not an positiv integer, the function returns a warning. 
    if(!Number.isInteger(number1) || !Number.isInteger(number2) || number1 <= 0 || number2 <= 0 || number1===number2 ){
        console.log('Please enter two different positive integers');
        return false
    }

    // If number1<number2, exchange them.
    if(number1 > number2){
        result = calculateGCR(number1, number2)
    }
    else {
        result = calculateGCR(number2, number1)
    }

    // The Euclidean algorithm
    // https://sites.math.rutgers.edu/~greenfie/gs2004/euclid.html#:~:text=The%20Euclidean%20algorithm%20is%20a,%3D1%C2%B730%2B15.
    function calculateGCR(num1, num2){
        let dividend = num1;
        let divisor = num2;
        let remainder;
        
        while (remainder !== 0){
            remainder = dividend % divisor;
            dividend = divisor;
            divisor = remainder;           
        }
        return dividend
    }

    console.log('The greatest common divisor of ' + number1 + ' and ' + number2 + ' is ' + result);
    return result
}

findGCR(8, 6);
findGCR(42, 56);
findGCR(96, 132);
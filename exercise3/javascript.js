function calculate(){
    let myInput = document.getElementById("myInput")
    let value = myInput.value;
    let area;
    if (!value){
        alert('Please enter a number!!!');
    }
    else if(isNaN(value)){
        alert("Please enter a valid number like 12, 20.8");
    }
    else {
        value = parseInt(value);
        console.log(Math.PI);
        area = value * value * (Math.PI);
        console.log(area);
        alert('Area of the circle is ' + area + 'm2');
    }

}
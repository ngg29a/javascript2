let a = 0;

function changeMode(){
    let modeButton = document.getElementById('mode');
    let body = document.getElementById('body');
    if(a % 2 === 0){
        body.style.color = '#1d2124';
        body.style.backgroundColor = 'white';
    }else{
        body.style.color = 'white';
        body.style.backgroundColor = '#1d2124';
    }
    a++;
}

function myToggle(element){
    element.parentElement.style.display = 'none';
}